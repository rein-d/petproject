package ru.rein.petproject.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rein.petproject.exception.EntityConvertException;
import ru.rein.petproject.exception.EntityNotFoundException;
import ru.rein.petproject.mapper.BookMapper;
import ru.rein.petproject.model.Book;
import ru.rein.petproject.model.dto.BookDto;
import ru.rein.petproject.repository.BookRepository;
import ru.rein.petproject.service.BookService;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final BookMapper bookMapper;

    public BookServiceImpl(BookRepository bookRepository, BookMapper bookMapper) {
        this.bookRepository = bookRepository;
        this.bookMapper = bookMapper;
    }

    @Override
    public Book getEntityById(Long id) {
        return bookRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(id));
    }

    @Override
    public BookDto getDtoById(Long id) {
        return bookRepository.findById(id)
                .map(bookMapper::toDto)
                .orElseThrow(() -> new EntityNotFoundException(id));
    }

    @Override
    public List<Book> getAllEntity() {
        return bookRepository.findAll();
    }

    @Override
    public List<BookDto> getAllDto() {
        return bookRepository.findAll().stream()
                .map(bookMapper::toDto).toList();
    }

    @Override
    public Book saveEntity(Book entity) {
        return bookRepository.save(entity);
    }

    @Override
    public BookDto saveDto(BookDto dto) {
        return bookMapper.toDto(
                bookRepository.saveAndFlush(
                        Optional.ofNullable(bookMapper.toEntity(dto))
                                .orElseThrow(() -> new EntityConvertException(dto))
                )
        );
    }

    @Override
    public List<Book> saveAllEntity(List<Book> entities) {
        return bookRepository.saveAll(entities);
    }

    @Override
    public List<Book> saveAllDto(List<BookDto> dto) {
        return List.of();
    }

    @Override
    public void deleteEntity(Long id) {
        bookRepository.deleteById(id);
    }
}
