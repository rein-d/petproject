package ru.rein.petproject.service;

import ru.rein.petproject.model.Book;
import ru.rein.petproject.model.dto.BookDto;

public interface BookService extends BaseService<Book, BookDto> {

}
