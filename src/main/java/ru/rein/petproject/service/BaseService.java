package ru.rein.petproject.service;

import java.util.List;

public interface BaseService<E, D> {

    E getEntityById(Long id);
    D getDtoById(Long id);
    List<E> getAllEntity();
    List<D> getAllDto();

    E saveEntity(E entity);
    D saveDto(D dto);
    List<E> saveAllEntity(List<E> entities);
    List<E> saveAllDto(List<D> dto);

    void deleteEntity(Long id);
}
