package ru.rein.petproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rein.petproject.model.Library;

public interface LibraryRepository extends JpaRepository<Library, Long> {

}
