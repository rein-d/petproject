package ru.rein.petproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rein.petproject.model.Author;

public interface AuthorRepository extends JpaRepository<Author, Long> {
}
