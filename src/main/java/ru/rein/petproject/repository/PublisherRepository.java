package ru.rein.petproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rein.petproject.model.Publisher;

public interface PublisherRepository extends JpaRepository<Publisher, Long> {
}
