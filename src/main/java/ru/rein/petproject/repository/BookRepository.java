package ru.rein.petproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rein.petproject.model.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
}
