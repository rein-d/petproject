package ru.rein.petproject.mapper;

import org.mapstruct.*;
import ru.rein.petproject.model.Library;
import ru.rein.petproject.model.dto.LibraryDto;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface LibraryMapper {
    Library toEntity(LibraryDto libraryDto);

    LibraryDto toDto(Library library);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Library partialUpdate(LibraryDto libraryDto, @MappingTarget Library library);
}