package ru.rein.petproject;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableEncryptableProperties
public class PetprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetprojectApplication.class, args);
	}

}
