package ru.rein.petproject.constants;

import java.util.Random;

public enum OrganizationType {
    IP,
    OOO,
    OAO,
    ZAO;

    private static final Random PRNG = new Random();

    public static OrganizationType random()  {
        OrganizationType[] directions = values();
        return directions[PRNG.nextInt(directions.length)];
    }
}
