package ru.rein.petproject.constants;

import java.util.Random;

public enum CoverType {
    SOFT,
    HARD;

    private static final Random PRNG = new Random();

    public static CoverType random()  {
        CoverType[] directions = values();
        return directions[PRNG.nextInt(directions.length)];
    }
}
