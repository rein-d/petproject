package ru.rein.petproject.model.dto;

/**
 * DTO for {@link ru.rein.petproject.model.Author}
 */
public record AuthorDto(
        Long id,
        String firstName,
        String lastName,
        String email,
        String phone
) {
  }