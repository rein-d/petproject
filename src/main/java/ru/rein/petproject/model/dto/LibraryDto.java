package ru.rein.petproject.model.dto;

import ru.rein.petproject.constants.OrganizationType;

/**
 * DTO for {@link ru.rein.petproject.model.Library}
 */
public record LibraryDto(
        Long id,
        String name,
        String address,
        String inn,
        String phone,
        OrganizationType type
) {
}