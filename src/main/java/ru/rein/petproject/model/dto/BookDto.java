package ru.rein.petproject.model.dto;

import ru.rein.petproject.constants.CoverType;

/**
 * DTO for {@link ru.rein.petproject.model.Book}
 */
public record BookDto(
        Long id,
        String title,
        String urlImage,
        CoverType cover,
        AuthorDto author,
        PublisherDto publisher,
        LibraryDto library
) {
  }