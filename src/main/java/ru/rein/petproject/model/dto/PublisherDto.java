package ru.rein.petproject.model.dto;

import ru.rein.petproject.constants.OrganizationType;

/**
 * DTO for {@link ru.rein.petproject.model.Publisher}
 */
public record PublisherDto(
        Long id,
        String companyName,
        String inn,
        OrganizationType organizationType,
        String name,
        String address,
        String phone,
        OrganizationType type
) {
}