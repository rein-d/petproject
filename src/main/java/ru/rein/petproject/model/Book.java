package ru.rein.petproject.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.proxy.HibernateProxy;
import ru.rein.petproject.constants.CoverType;
import ru.rein.petproject.model.baseEntities.BaseEntity;

import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Book extends BaseEntity {

    private String title;

    private String urlImage;

    @Enumerated(EnumType.STRING)
    private CoverType cover;

    @ManyToOne
    private Author author;

    @ManyToOne
    private Publisher publisher;

    @ManyToOne
    private Library library;

}
