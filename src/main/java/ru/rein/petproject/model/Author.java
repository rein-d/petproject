package ru.rein.petproject.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.proxy.HibernateProxy;
import ru.rein.petproject.model.baseEntities.BaseEntity;

import java.util.Objects;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Author extends BaseEntity {

    private String firstName;

    private String lastName;

    private String email;

    private String phone;

}
