package ru.rein.petproject.model.baseEntities;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.rein.petproject.constants.OrganizationType;

@Setter
@Getter
@AllArgsConstructor
@MappedSuperclass
@NoArgsConstructor
public class BaseOrganizationEntity extends BaseEntity {
    private String inn;
    private String name;
    private String address;
    private String phone;

    @Enumerated(EnumType.STRING)
    private OrganizationType type;
}
