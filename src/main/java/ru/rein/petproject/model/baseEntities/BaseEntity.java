package ru.rein.petproject.model.baseEntities;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public class BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Override
    public boolean equals(Object o) {
        // Быстрая проверка на идентичность объектов
        if (this == o) return true;

        // Проверка на тип и null
        if (o == null || getClass() != o.getClass()) return false;

        BaseEntity that = (BaseEntity) o;

        // Если у обоих объектов нет id, то они разные
        if (id == null && that.id == null) return false;

        // Если один из объектов не сохранен, считаем их разными
        if (id == null || that.id == null) return false;

        // Сравнение по ID для персистентных объектов
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        // Если объект еще не сохранен, возвращаем временный hash
        return (id != null) ? id.hashCode() : System.identityHashCode(this);
    }
}
