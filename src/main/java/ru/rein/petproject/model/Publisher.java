package ru.rein.petproject.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.proxy.HibernateProxy;
import ru.rein.petproject.model.baseEntities.BaseOrganizationEntity;

import java.util.Objects;

@Getter
@Setter
@ToString
@AllArgsConstructor
@Entity
public class Publisher extends BaseOrganizationEntity {

}
