package ru.rein.petproject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.rein.petproject.model.dto.BookDto;
import ru.rein.petproject.service.BookService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/books")
@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
public class BooksController {
    private final BookService bookService;

    @GetMapping
    public ResponseEntity<List<BookDto>> getAllBooks() {
        return ResponseEntity.ok(bookService.getAllDto());
    }

    @PostMapping
    @PreAuthorize("hasAuthority('admin:create')")
    public ResponseEntity<BookDto> addBook(@RequestBody BookDto book) {
        return ResponseEntity.ok(bookService.saveDto(book));
    }
}
