package ru.rein.petproject.util;

import org.springframework.boot.CommandLineRunner;
import ru.rein.petproject.constants.CoverType;
import ru.rein.petproject.constants.OrganizationType;
import ru.rein.petproject.model.Author;
import ru.rein.petproject.model.Book;
import ru.rein.petproject.model.Library;
import ru.rein.petproject.model.Publisher;
import ru.rein.petproject.repository.AuthorRepository;
import ru.rein.petproject.repository.BookRepository;
import ru.rein.petproject.repository.LibraryRepository;
import ru.rein.petproject.repository.PublisherRepository;

import java.util.ArrayList;

//@Component
public class DataInitializer implements CommandLineRunner {
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final LibraryRepository libraryRepository;
    private final PublisherRepository publisherRepository;

    public DataInitializer(BookRepository bookRepository, AuthorRepository authorRepository, LibraryRepository libraryRepository, PublisherRepository publisherRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
        this.libraryRepository = libraryRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void run(String... args) {
        var books = new ArrayList<Book>();
        for (int i = 0; i < 10; i++) {
            var book = new Book();
            var library = new Library();
            var publisher = new Publisher();
            var author = new Author();

            library.setName("Library " + i);
            library.setAddress("Address " + i);
            library.setInn("INN " + i);
            library.setType(OrganizationType.random());
            library.setPhone("Phone " + i);
            library = libraryRepository.save(library);

            publisher.setName("Publisher " + i);
            publisher.setAddress("Address " + i);
            publisher.setInn("INN " + i);
            publisher.setType(OrganizationType.random());
            publisher.setPhone("Phone " + i);
            publisher = publisherRepository.save(publisher);

            author.setPhone("Phone " + i);
            author.setEmail("Email " + i);
            author.setFirstName("FirstName" + i);
            author.setLastName("LastName" + i);
            author = authorRepository.save(author);

            book.setTitle("Title " + i);
            book.setCover(CoverType.random());
            book.setUrlImage("UrlImage " + i);
            book.setPublisher(publisher);
            book.setAuthor(author);
            book.setLibrary(library);

            books.add(book);
        }
        bookRepository.saveAll(books);
    }
}
