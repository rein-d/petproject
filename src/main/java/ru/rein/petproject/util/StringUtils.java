package ru.rein.petproject.util;

import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class StringUtils {

    private StringUtils() {
    }

    public static String beautifyName(String name) {
        if (Objects.nonNull(name) && !name.isEmpty()) {
            name = name.trim();
            name = name.substring(0, 1).toUpperCase() + name.toLowerCase().substring(1);
        }
        return name;
    }
}
