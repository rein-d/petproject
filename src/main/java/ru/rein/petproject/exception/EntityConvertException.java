package ru.rein.petproject.exception;

public class EntityConvertException extends RuntimeException {
    public EntityConvertException(Object obj) {
        super(String.format("Cannot convert %s to entity", obj));
    }
}
