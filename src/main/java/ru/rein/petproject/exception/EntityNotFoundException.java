package ru.rein.petproject.exception;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(long id) {
        super(String.format("Entity for id %d not found", id));
    }
}
